# Install Epitech header for vim

## Install vim plugin manager

Execute this following command in a terminal

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Execute the following commands

```
echo -e "call plug#begin()\n" >> ~/.vimrc
echo "Plug 'Nero-F/vim-tek-header'" >> ~/.vimrc
echo -e "\ncall plug#end()" >> ~/.vimrc
echo -e "\ncall plug#end()" >> ~/.vimrc
echo "let mapleader=\",\"" >> ~/.vimrc
```

## Install vim plugins

Start vim

```
vim
```

Type the following in vim

```
:PlugInstall
```

When seeing `Finishing ... Done!`, restart vim (`:q` for quit)

## Usage

To use the plugin, open a `.c` file

In `normal` mode type `,h`  
Follow the displayed instructions

---

Pour plus d'informations, cliquez [ici](https://github.com/Nero-F/vim-tek-header)

Si vous n'arrivez pas à installer le header Epitech, demandez **Corentin** à un AER ;)

# Install Docker

Execute this following command in a terminal

```
sudo dnf install -y docker
```

Execute the following commands

```
sudo systemctl enable docker
sudo systemctl start docker
```
